#!/bin/bash

UNZIP_DIR=Payload

if [ $# != 2 ]; then
	echo "missing args"
	exit 0
fi
if [ ! -f $1 ]; then
	echo "Missing file" $1
fi
FILENAME=$1
for i in `seq 2 $#`
do
    eval IDENTITY=\$$i
done

result=$(file $FILENAME)
if [[ ! $result =~ "Zip" ]]; then
	echo "File is not an archive"
	echo $result
	exit 0
fi

unzip $FILENAME
DIRS=`ls -l | egrep '^d' | awk '{print $9}'`
for DIR in $DIRS; do
	if [ "$DIR" = "$UNZIP_DIR" ]; then
		APPFILES=`ls -l $DIR | egrep '.app' | awk '{print $9}'`
		for APPFILE in $APPFILES; do
			codesign -f -s "$IDENTITY" $DIR"/"$APPFILE
		done
	fi
done
zip -r $FILENAME $UNZIP_DIR

